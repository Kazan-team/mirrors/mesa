
/*
 * Copyright © 2016 Red Hat.
 * Copyright © 2016 Bas Nieuwenhuizen
 *
 * based in part on anv driver which is:
 * Copyright © 2015 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "vk_util.h"
#include "libresoc_private.h"


VkResult
libresoc_image_create(VkDevice _device,
		  const struct libresoc_image_create_info *create_info,
		  const VkAllocationCallbacks* alloc,
		  VkImage *pImage)
{
	LIBRESOC_FROM_HANDLE(libresoc_device, device, _device);
	const VkImageCreateInfo *pCreateInfo = create_info->vk_info;
	struct libresoc_image *image = NULL;
	assert(pCreateInfo->sType == VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO);

	// const unsigned plane_count = vk_format_get_plane_count(format);
	// const size_t image_struct_size = sizeof(*image) + sizeof(struct libresoc_image_plane) * plane_count;

	const size_t image_struct_size = sizeof(*image);
	// libresoc_assert(pCreateInfo->mipLevels > 0);
	// libresoc_assert(pCreateInfo->arrayLayers > 0);
	// libresoc_assert(pCreateInfo->samples > 0);
	// libresoc_assert(pCreateInfo->extent.width > 0);
	// libresoc_assert(pCreateInfo->extent.height > 0);
	// libresoc_assert(pCreateInfo->extent.depth > 0);

	image = vk_zalloc2(&device->vk.alloc, alloc, image_struct_size, 8,
			   VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);
	if (!image)
		return vk_error(device->instance, VK_ERROR_OUT_OF_HOST_MEMORY);

	vk_object_base_init(&device->vk, &image->base, VK_OBJECT_TYPE_IMAGE);

	image->type = pCreateInfo->imageType;
	// image->info.width = pCreateInfo->extent.width;
	// image->info.height = pCreateInfo->extent.height;
	// image->info.depth = pCreateInfo->extent.depth;
	// image->info.samples = pCreateInfo->samples;
	// image->info.storage_samples = pCreateInfo->samples;
	// image->info.array_size = pCreateInfo->arrayLayers;
	// image->info.levels = pCreateInfo->mipLevels;
	// image->info.num_channels = vk_format_get_nr_components(format);

	//image->vk_format = format;
	image->tiling = pCreateInfo->tiling;
	image->usage = pCreateInfo->usage;
	image->flags = pCreateInfo->flags;
	//image->plane_count = plane_count;

	image->exclusive = pCreateInfo->sharingMode == VK_SHARING_MODE_EXCLUSIVE;
	// if (pCreateInfo->sharingMode == VK_SHARING_MODE_CONCURRENT) {
	// 	for (uint32_t i = 0; i < pCreateInfo->queueFamilyIndexCount; ++i)
	// 		if (pCreateInfo->pQueueFamilyIndices[i] == VK_QUEUE_FAMILY_EXTERNAL ||
	// 		    pCreateInfo->pQueueFamilyIndices[i] == VK_QUEUE_FAMILY_FOREIGN_EXT)
	// 			image->queue_family_mask |= (1u << LIBRESOC_MAX_QUEUE_FAMILIES) - 1u;
	// 		else
	// 			image->queue_family_mask |= 1u << pCreateInfo->pQueueFamilyIndices[i];
	// }

	// const VkExternalMemoryImageCreateInfo *external_info =
	// 	vk_find_struct_const(pCreateInfo->pNext,
	// 	                     EXTERNAL_MEMORY_IMAGE_CREATE_INFO) ;

	// image->shareable = external_info;
	// if (!vk_format_is_depth_or_stencil(format) && !image->shareable) {
	// 	image->info.surf_index = &device->image_mrt_offset_counter;
	// }

	// for (unsigned plane = 0; plane < image->plane_count; ++plane) {
	// 	image->planes[plane].surface.flags =
	// 		libresoc_get_surface_flags(device, image, plane, pCreateInfo, format);
	// }

	// bool delay_layout = external_info &&
	// 	(external_info->handleTypes & VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID);

	// if (delay_layout) {
	// 	*pImage = libresoc_image_to_handle(image);
	// 	assert (!(image->flags & VK_IMAGE_CREATE_SPARSE_BINDING_BIT));
	// 	return VK_SUCCESS;
	// }

	// ASSERTED VkResult result = libresoc_image_create_layout(device, *create_info, image);
	// assert(result == VK_SUCCESS);

	// if (image->flags & VK_IMAGE_CREATE_SPARSE_BINDING_BIT) {
	// 	image->alignment = MAX2(image->alignment, 4096);
	// 	image->size = align64(image->size, image->alignment);
	// 	image->offset = 0;

	// 	image->bo = device->ws->buffer_create(device->ws, image->size, image->alignment,
	// 	                                      0, RADEON_FLAG_VIRTUAL, LIBRESOC_BO_PRIORITY_VIRTUAL);
	// 	if (!image->bo) {
	// 		libresoc_destroy_image(device, alloc, image);
	// 		return vk_error(device->instance, VK_ERROR_OUT_OF_DEVICE_MEMORY);
	// 	}
	// }

	*pImage = libresoc_image_to_handle(image);

	return VK_SUCCESS;
}


VkResult
libresoc_CreateImage(VkDevice device,
		 const VkImageCreateInfo *pCreateInfo,
		 const VkAllocationCallbacks *pAllocator,
		 VkImage *pImage)
{

	const struct wsi_image_create_info *wsi_info =
		vk_find_struct_const(pCreateInfo->pNext, WSI_IMAGE_CREATE_INFO_MESA);
	bool scanout = wsi_info && wsi_info->scanout;

	return libresoc_image_create(device,
				 &(struct libresoc_image_create_info) {
					 .vk_info = pCreateInfo,
					 .scanout = scanout,
				 },
				 pAllocator,
				 pImage);
}
void libresoc_GetImageSubresourceLayout(
	VkDevice                                    _device,
	VkImage                                     _image,
	const VkImageSubresource*                   pSubresource,
	VkSubresourceLayout*                        pLayout)
{
//TODO: stub
}

VkResult
libresoc_CreateImageView(VkDevice _device,
		     const VkImageViewCreateInfo *pCreateInfo,
		     const VkAllocationCallbacks *pAllocator,
		     VkImageView *pView)
{
	//TODO: stub
	return VK_SUCCESS;
	// LIBRESOC_FROM_HANDLE(libresoc_device, device, _device);
	// struct libresoc_image_view *view;

	// view = vk_alloc2(&device->vk.alloc, pAllocator, sizeof(*view), 8,
	// 		   VK_SYSTEM_ALLOCATION_SCOPE_OBJECT);
	// if (view == NULL)
	// 	return vk_error(device->instance, VK_ERROR_OUT_OF_HOST_MEMORY);

	// vk_object_base_init(&device->vk, &view->base,
	// 		    VK_OBJECT_TYPE_IMAGE_VIEW);

	// libresoc_image_view_init(view, device, pCreateInfo, NULL);

	// *pView = libresoc_image_view_to_handle(view);

	// return VK_SUCCESS;
}
void
libresoc_DestroyImage(VkDevice _device, VkImage _image,
		  const VkAllocationCallbacks *pAllocator)
{

}

void
libresoc_DestroyImageView(VkDevice _device, VkImageView _iview,
		      const VkAllocationCallbacks *pAllocator)
{}
