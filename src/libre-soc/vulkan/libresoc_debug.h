/*
 * Copyright © 2017 Google.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef LIBRESOC_DEBUG_H
#define LIBRESOC_DEBUG_H

#include "libresoc_private.h"

/* Please keep docs/envvars.rst up-to-date when you add/remove options. */
enum {
	LIBRESOC_DEBUG_NO_FAST_CLEARS    = 1 << 0,
	LIBRESOC_DEBUG_NO_DCC            = 1 << 1,
	LIBRESOC_DEBUG_DUMP_SHADERS      = 1 << 2,
	LIBRESOC_DEBUG_NO_CACHE          = 1 << 3,
	LIBRESOC_DEBUG_DUMP_SHADER_STATS = 1 << 4,
	LIBRESOC_DEBUG_NO_HIZ            = 1 << 5,
	LIBRESOC_DEBUG_NO_COMPUTE_QUEUE  = 1 << 6,
	LIBRESOC_DEBUG_ALL_BOS           = 1 << 7,
	LIBRESOC_DEBUG_NO_IBS            = 1 << 8,
	LIBRESOC_DEBUG_DUMP_SPIRV        = 1 << 9,
	LIBRESOC_DEBUG_VM_FAULTS         = 1 << 10,
	LIBRESOC_DEBUG_ZERO_VRAM         = 1 << 11,
	LIBRESOC_DEBUG_SYNC_SHADERS      = 1 << 12,
	LIBRESOC_DEBUG_PREOPTIR          = 1 << 13,
	LIBRESOC_DEBUG_NO_DYNAMIC_BOUNDS = 1 << 14,
	LIBRESOC_DEBUG_NO_OUT_OF_ORDER   = 1 << 15,
	LIBRESOC_DEBUG_INFO              = 1 << 16,
	LIBRESOC_DEBUG_ERRORS            = 1 << 17,
	LIBRESOC_DEBUG_STARTUP           = 1 << 18,
	LIBRESOC_DEBUG_CHECKIR           = 1 << 19,
	LIBRESOC_DEBUG_NOTHREADLLVM      = 1 << 20,
	LIBRESOC_DEBUG_NOBINNING         = 1 << 21,
	LIBRESOC_DEBUG_NO_NGG            = 1 << 22,
	LIBRESOC_DEBUG_ALL_ENTRYPOINTS   = 1 << 23,
	LIBRESOC_DEBUG_DUMP_META_SHADERS = 1 << 24,
	LIBRESOC_DEBUG_NO_MEMORY_CACHE   = 1 << 25,
	LIBRESOC_DEBUG_DISCARD_TO_DEMOTE = 1 << 26,
	LIBRESOC_DEBUG_LLVM              = 1 << 27,
	LIBRESOC_DEBUG_FORCE_COMPRESS    = 1 << 28,
	LIBRESOC_DEBUG_DUMP_NIR          = 1 << 29,
};

enum {
	LIBRESOC_PERFTEST_LOCAL_BOS       = 1 << 0,
	LIBRESOC_PERFTEST_DCC_MSAA        = 1 << 1,
	LIBRESOC_PERFTEST_BO_LIST         = 1 << 2,
	LIBRESOC_PERFTEST_TC_COMPAT_CMASK = 1 << 3,
	LIBRESOC_PERFTEST_CS_WAVE_32      = 1 << 4,
	LIBRESOC_PERFTEST_PS_WAVE_32      = 1 << 5,
	LIBRESOC_PERFTEST_GE_WAVE_32      = 1 << 6,
	LIBRESOC_PERFTEST_DFSM            = 1 << 7,
};

void
libresoc_print_spirv(const char *data, uint32_t size, FILE *fp);

#endif
