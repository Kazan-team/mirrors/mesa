/*
 * Copyright © 2019 Raspberry Pi
 *
 * based in part on anv driver which is:
 * Copyright © 2015 Intel Corporation
 *
 * based in part on libresoc driver which is:
 * Copyright © 2016 Red Hat.
 * Copyright © 2016 Bas Nieuwenhuizen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef LIBRESOC_PRIVATE_H
#define LIBRESOC_PRIVATE_H

#include <stdio.h>
#include <string.h>
#include <vulkan/vulkan.h>
#include <vulkan/vk_icd.h>

#include "vk_alloc.h"
#include "vk_debug_report.h"
#include "util/xmlconfig.h"
#include "compiler/shader_enums.h"

#include "vk_object.h"
#include "libresoc_entrypoints.h"
#include "libresoc_extensions.h"
#include "libresoc_constants.h"
#include "libresoc_debug.h"

#include "wsi_common.h"
#define LIBRESOC_MAX_QUEUE_FAMILIES 1 

static inline gl_shader_stage
vk_to_mesa_shader_stage(VkShaderStageFlagBits vk_stage)
{
	assert(__builtin_popcount(vk_stage) == 1);
	return ffs(vk_stage) - 1;
}

static inline VkShaderStageFlagBits
mesa_to_vk_shader_stage(gl_shader_stage mesa_stage)
{
	return (1 << mesa_stage);
}

static inline uint32_t
align_u32(uint32_t v, uint32_t a)
{
	assert(a != 0 && a == (a & -a));
	return (v + a - 1) & ~(a - 1);
}

static inline uint32_t
align_u32_npot(uint32_t v, uint32_t a)
{
	return (v + a - 1) / a * a;
}

static inline uint64_t
align_u64(uint64_t v, uint64_t a)
{
	assert(a != 0 && a == (a & -a));
	return (v + a - 1) & ~(a - 1);
}

static inline int32_t
align_i32(int32_t v, int32_t a)
{
	assert(a != 0 && a == (a & -a));
	return (v + a - 1) & ~(a - 1);
}

/** Alignment must be a power of 2. */
static inline bool
libresoc_is_aligned(uintmax_t n, uintmax_t a)
{
	assert(a == (a & -a));
	return (n & (a - 1)) == 0;
}

static inline uint32_t
round_up_u32(uint32_t v, uint32_t a)
{
	return (v + a - 1) / a;
}

static inline uint64_t
round_up_u64(uint64_t v, uint64_t a)
{
	return (v + a - 1) / a;
}

static inline uint32_t
libresoc_minify(uint32_t n, uint32_t levels)
{
	if (unlikely(n == 0))
		return 0;
	else
		return MAX2(n >> levels, 1);
}
static inline float
libresoc_clamp_f(float f, float min, float max)
{
	assert(min < max);

	if (f > max)
		return max;
	else if (f < min)
		return min;
	else
		return f;
}

static inline bool
libresoc_clear_mask(uint32_t *inout_mask, uint32_t clear_mask)
{
	if (*inout_mask & clear_mask) {
		*inout_mask &= ~clear_mask;
		return true;
	} else {
		return false;
	}
}

struct libresoc_fence {
	struct vk_object_base base;
};

struct libresoc_image_create_info {
	const VkImageCreateInfo *vk_info;
	bool scanout;
	bool no_metadata_planes;
};

struct libresoc_image {
	struct vk_object_base base;
	VkImageType type;
	/* The original VkFormat provided by the client.  This may not match any
	 * of the actual surface formats.
	 */
	VkFormat vk_format;
	VkImageAspectFlags aspects;
	VkImageUsageFlags usage; /**< Superset of VkImageCreateInfo::usage. */
	VkImageTiling tiling; /** VkImageCreateInfo::tiling */
	VkImageCreateFlags flags; /** VkImageCreateInfo::flags */

	VkDeviceSize size;
	uint32_t alignment;

	unsigned queue_family_mask;
	bool exclusive;
	bool shareable;

};

VkResult libresoc_image_create(VkDevice _device,
			   const struct libresoc_image_create_info *info,
			   const VkAllocationCallbacks* alloc,
			   VkImage *pImage);

struct libresoc_cmd_pool {
	struct vk_object_base                        base;
	VkAllocationCallbacks                        alloc;
	struct list_head                             cmd_buffers;
	struct list_head                             free_cmd_buffers;
	uint32_t queue_family_index;
};

struct libresoc_semaphore {
	struct vk_object_base base;
};

struct libresoc_instance;
struct libresoc_device;
struct cache_entry;

struct libresoc_pipeline_cache {
	struct vk_object_base                        base;
	struct libresoc_device *                         device;
	pthread_mutex_t                              mutex;
	VkPipelineCacheCreateFlags                   flags;

	uint32_t                                     total_size;
	uint32_t                                     table_size;
	uint32_t                                     kernel_count;
	struct cache_entry **                        hash_table;
	bool                                         modified;

	VkAllocationCallbacks                        alloc;
};


struct libresoc_shader_binary;
struct libresoc_shader_variant;

struct libresoc_pipeline {
	struct vk_object_base                         base;
	struct libresoc_device *                          device;


	VkShaderStageFlags                           active_stages;

};
void
libresoc_pipeline_cache_init(struct libresoc_pipeline_cache *cache,
			 struct libresoc_device *device);
void
libresoc_pipeline_cache_finish(struct libresoc_pipeline_cache *cache);
bool
libresoc_pipeline_cache_load(struct libresoc_pipeline_cache *cache,
			 const void *data, size_t size);

bool
libresoc_create_shader_variants_from_pipeline_cache(struct libresoc_device *device,
					        struct libresoc_pipeline_cache *cache,
					        const unsigned char *sha1,
					        struct libresoc_shader_variant **variants,
						bool *found_in_application_cache);

void
libresoc_pipeline_cache_insert_shaders(struct libresoc_device *device,
				   struct libresoc_pipeline_cache *cache,
				   const unsigned char *sha1,
				   struct libresoc_shader_variant **variants,
				   struct libresoc_shader_binary *const *binaries);

VkResult libresoc_init_wsi(struct libresoc_physical_device *physical_device);
void libresoc_finish_wsi(struct libresoc_physical_device *physical_device);

struct libresoc_device {

   struct vk_device vk;
   VkAllocationCallbacks alloc;

   struct libresoc_instance *instance;

   struct libresoc_device_extension_table enabled_extensions;
   struct libresoc_device_dispatch_table dispatch;

   struct libresoc_queue *queues[LIBRESOC_MAX_QUEUE_FAMILIES];
   int queue_count[LIBRESOC_MAX_QUEUE_FAMILIES];
   struct radeon_cmdbuf *empty_cs[LIBRESOC_MAX_QUEUE_FAMILIES];
   struct libresoc_physical_device                  *physical_device;

   /* Backup in-memory cache to be used if the app doesn't provide one */
   struct libresoc_pipeline_cache *                mem_cache;
   /* Condition variable for legacy timelines, to notify waiters when a
    * new point gets submitted. */
   pthread_cond_t timeline_cond;
   /* Overallocation. */
   bool overallocation_disallowed;
   uint64_t allocated_memory_size[VK_MAX_MEMORY_HEAPS];
   mtx_t overallocation_mutex;
   /* FIXME: stub */
};


struct libresoc_physical_device {
   VK_LOADER_DATA _loader_data;

   struct list_head                            link;
   struct libresoc_instance *instance;
   struct wsi_device wsi_device;
   struct libresoc_device_extension_table supported_extensions;
   struct libresoc_physical_device_dispatch_table dispatch;

   char                                        name[VK_MAX_PHYSICAL_DEVICE_NAME_SIZE];
   uint8_t                                     driver_uuid[VK_UUID_SIZE];
   uint8_t                                     device_uuid[VK_UUID_SIZE];
   uint8_t                                     cache_uuid[VK_UUID_SIZE];
   int local_fd;
   int master_fd;
   VkPhysicalDeviceMemoryProperties memory_properties;
   /* FIXME: stub */
};

struct libresoc_app_info {
   const char *app_name;
   uint32_t app_version;
   const char *engine_name;
   uint32_t engine_version;
   uint32_t api_version;
};

struct libresoc_instance {
   struct vk_object_base                       base;

   VkAllocationCallbacks alloc;

   uint32_t                                    apiVersion;

   uint64_t debug_flags;
   char *                                      engineName;
   uint32_t                                    engineVersion;
   struct libresoc_app_info app_info;

   bool                                        physical_devices_enumerated;
   struct libresoc_instance_extension_table enabled_extensions;
   struct libresoc_instance_dispatch_table dispatch;
   struct libresoc_device_dispatch_table device_dispatch;
   struct libresoc_physical_device_dispatch_table   physical_device_dispatch;
   int physical_device_count;
   struct list_head physical_devices;

   struct driOptionCache dri_options;
   struct driOptionCache available_dri_options;
   struct vk_debug_report_instance debug_report_callbacks;
};

struct libresoc_deferred_queue_submission;
struct libresoc_queue {
   VK_LOADER_DATA _loader_data;

   struct libresoc_device *device;

   uint32_t queue_family_index;
   int queue_idx;
   VkDeviceQueueCreateFlags flags;

   struct list_head pending_submissions;
   pthread_mutex_t pending_mutex;
   pthread_mutex_t thread_mutex;
   pthread_cond_t thread_cond;
   //struct libresoc_deferred_queue_submission *thread_submission;
   pthread_t submission_thread;
   bool thread_exit;
   bool thread_running;
   /* FIXME: stub */
};

struct libresoc_cmd_buffer_upload {
	uint8_t *map;
	unsigned offset;
	uint64_t size;
	struct list_head list;
};

enum libresoc_cmd_buffer_status {
	LIBRESOC_CMD_BUFFER_STATUS_INVALID,
	LIBRESOC_CMD_BUFFER_STATUS_INITIAL,
	LIBRESOC_CMD_BUFFER_STATUS_RECORDING,
	LIBRESOC_CMD_BUFFER_STATUS_EXECUTABLE,
	LIBRESOC_CMD_BUFFER_STATUS_PENDING,
};

struct libresoc_cmd_buffer {
	struct vk_object_base                         base;

	struct libresoc_device *                          device;

	struct libresoc_cmd_pool *                        pool;
	struct list_head                             pool_link;

	VkCommandBufferUsageFlags                    usage_flags;
	VkCommandBufferLevel                         level;
	enum libresoc_cmd_buffer_status status;
	//struct radeon_cmdbuf *cs;
	// struct libresoc_cmd_state state;
	// struct libresoc_vertex_binding                   vertex_bindings[MAX_VBS];
	// struct libresoc_streamout_binding                streamout_bindings[MAX_SO_BUFFERS];
	uint32_t queue_family_index;

	uint8_t push_constants[MAX_PUSH_CONSTANTS_SIZE];
	VkShaderStageFlags push_constant_stages;
	// struct libresoc_descriptor_set meta_push_descriptors;

	// struct libresoc_descriptor_state descriptors[MAX_BIND_POINTS];

	struct libresoc_cmd_buffer_upload upload;

	uint32_t scratch_size_per_wave_needed;
	uint32_t scratch_waves_wanted;
	uint32_t compute_scratch_size_per_wave_needed;
	uint32_t compute_scratch_waves_wanted;
	uint32_t esgs_ring_size_needed;
	uint32_t gsvs_ring_size_needed;
	bool tess_rings_needed;
	bool sample_positions_needed;

	VkResult record_result;

};

struct libresoc_device_memory {
	struct vk_object_base                        base;
	/* for dedicated allocations */
	struct libresoc_image                            *image;
	//struct libresoc_buffer                           *buffer;
	uint32_t                                     heap_index;
	uint64_t                                     alloc_size;
	void *                                       map;
	void *                                       user_ptr;
};

void libresoc_free_memory(struct libresoc_device *device,
		      const VkAllocationCallbacks* pAllocator,
		      struct libresoc_device_memory *mem);

uint32_t libresoc_physical_device_api_version(struct libresoc_physical_device *dev);

int libresoc_get_instance_entrypoint_index(const char *name);
int libresoc_get_device_entrypoint_index(const char *name);
int libresoc_get_physical_device_entrypoint_index(const char *name);

const char *libresoc_get_instance_entry_name(int index);
const char *libresoc_get_physical_device_entry_name(int index);
const char *libresoc_get_device_entry_name(int index);

bool
libresoc_instance_entrypoint_is_enabled(int index, uint32_t core_version,
                                    const struct libresoc_instance_extension_table *instance);
bool
libresoc_physical_device_entrypoint_is_enabled(int index, uint32_t core_version,
                                           const struct libresoc_instance_extension_table *instance);
bool
libresoc_device_entrypoint_is_enabled(int index, uint32_t core_version,
                                  const struct libresoc_instance_extension_table *instance,
                                  const struct libresoc_device_extension_table *device);

void *libresoc_lookup_entrypoint(const char *name);

const char *
libresoc_get_debug_option_name(int id);

struct libresoc_binning_settings {
	unsigned context_states_per_bin; /* allowed range: [1, 6] */
	unsigned persistent_states_per_bin; /* allowed range: [1, 32] */
	unsigned fpovs_per_batch; /* allowed range: [0, 255], 0 = unlimited */
};

struct libresoc_binning_settings
libresoc_get_binning_settings(const struct libresoc_physical_device *pdev);

struct vk_format_description;
uint32_t libresoc_translate_buffer_dataformat(const struct vk_format_description *desc,
					  int first_non_void);
uint32_t libresoc_translate_buffer_numformat(const struct vk_format_description *desc,
					 int first_non_void);
bool libresoc_is_buffer_format_supported(VkFormat format, bool *scaled);
uint32_t libresoc_translate_colorformat(VkFormat format);
uint32_t libresoc_translate_color_numformat(VkFormat format,
					const struct vk_format_description *desc,
					int first_non_void);
uint32_t libresoc_colorformat_endian_swap(uint32_t colorformat);
unsigned libresoc_translate_colorswap(VkFormat format, bool do_endian_swap);
uint32_t libresoc_translate_dbformat(VkFormat format);
uint32_t libresoc_translate_tex_dataformat(VkFormat format,
				       const struct vk_format_description *desc,
				       int first_non_void);
uint32_t libresoc_translate_tex_numformat(VkFormat format,
				      const struct vk_format_description *desc,
				      int first_non_void);
bool libresoc_format_pack_clear_color(VkFormat format,
				  uint32_t clear_vals[2],
				  VkClearColorValue *value);
bool libresoc_is_colorbuffer_format_supported(VkFormat format, bool *blendable);
bool libresoc_dcc_formats_compatible(VkFormat format1,
                                 VkFormat format2);
bool libresoc_device_supports_etc(struct libresoc_physical_device *physical_device);


/* Whether the image has a htile  that is known consistent with the contents of
 * the image and is allowed to be in compressed form.
 *
 * If this is false reads that don't use the htile should be able to return
 * correct results.
 */
bool libresoc_layout_is_htile_compressed(const struct libresoc_image *image,
                                     VkImageLayout layout,
                                     bool in_render_loop,
                                     unsigned queue_mask);

bool libresoc_layout_can_fast_clear(const struct libresoc_image *image,
			        VkImageLayout layout,
			        bool in_render_loop,
			        unsigned queue_mask);

bool libresoc_layout_dcc_compressed(const struct libresoc_device *device,
				const struct libresoc_image *image,
			        VkImageLayout layout,
			        bool in_render_loop,
			        unsigned queue_mask);
VkResult
libresoc_graphics_pipeline_create(VkDevice device,
			      VkPipelineCache cache,
			      const VkGraphicsPipelineCreateInfo *pCreateInfo,
			      const VkAllocationCallbacks *alloc,
			      VkPipeline *pPipeline);

#define libresoc_printflike(a, b) __attribute__((__format__(__printf__, a, b)))

VkResult __vk_errorf(struct libresoc_instance *instance, VkResult error,
                     const char *file, int line,
                     const char *format, ...);

#define vk_error(instance, error) __vk_errorf(instance, error, __FILE__, __LINE__, NULL);
#define vk_errorf(instance, error, format, ...) __vk_errorf(instance, error, __FILE__, __LINE__, format, ## __VA_ARGS__);

void libresoc_loge(const char *format, ...) libresoc_printflike(1, 2);
void libresoc_loge_v(const char *format, va_list va);

#define LIBRESOC_DEFINE_HANDLE_CASTS(__libresoc_type, __VkType)   \
                                                        \
   static inline struct __libresoc_type *                    \
   __libresoc_type ## _from_handle(__VkType _handle)         \
   {                                                    \
      return (struct __libresoc_type *) _handle;             \
   }                                                    \
                                                        \
   static inline __VkType                               \
   __libresoc_type ## _to_handle(struct __libresoc_type *_obj)    \
   {                                                    \
      return (__VkType) _obj;                           \
   }

#define LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(__libresoc_type, __VkType)		\
									\
	static inline struct __libresoc_type *				\
	__libresoc_type ## _from_handle(__VkType _handle)			\
	{								\
		return (struct __libresoc_type *)(uintptr_t) _handle;	\
	}								\
									\
	static inline __VkType						\
	__libresoc_type ## _to_handle(struct __libresoc_type *_obj)		\
	{								\
		return (__VkType)(uintptr_t) _obj;			\
	}

#define LIBRESOC_FROM_HANDLE(__libresoc_type, __name, __handle)			\
   struct __libresoc_type *__name = __libresoc_type ## _from_handle(__handle)

LIBRESOC_DEFINE_HANDLE_CASTS(libresoc_cmd_buffer, VkCommandBuffer)
LIBRESOC_DEFINE_HANDLE_CASTS(libresoc_device, VkDevice)
LIBRESOC_DEFINE_HANDLE_CASTS(libresoc_instance, VkInstance)
LIBRESOC_DEFINE_HANDLE_CASTS(libresoc_physical_device, VkPhysicalDevice)
LIBRESOC_DEFINE_HANDLE_CASTS(libresoc_queue, VkQueue)

LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_cmd_pool, VkCommandPool)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_buffer, VkBuffer)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_buffer_view, VkBufferView)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_descriptor_pool, VkDescriptorPool)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_descriptor_set, VkDescriptorSet)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_descriptor_set_layout, VkDescriptorSetLayout)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_descriptor_update_template, VkDescriptorUpdateTemplate)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_device_memory, VkDeviceMemory)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_fence, VkFence)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_event, VkEvent)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_framebuffer, VkFramebuffer)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_image, VkImage)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_image_view, VkImageView);
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_pipeline_cache, VkPipelineCache)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_pipeline, VkPipeline)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_pipeline_layout, VkPipelineLayout)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_query_pool, VkQueryPool)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_render_pass, VkRenderPass)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_sampler, VkSampler)
//LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_sampler_ycbcr_conversion, VkSamplerYcbcrConversion)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_shader_module, VkShaderModule)
LIBRESOC_DEFINE_NONDISP_HANDLE_CASTS(libresoc_semaphore, VkSemaphore)

#endif /* LIBRESOC_PRIVATE_H */
